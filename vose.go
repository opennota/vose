// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

// http://www.keithschwarz.com/darts-dice-coins/

// Package vose implements Walker-Vose alias algorithm for generating discrete random numbers
// with a given probability distribution.
package vose

// Vose is an RNG which generates discrete random numbers with a given probability distribution.
type Vose struct {
	prob  []float64
	alias []int
	rand  Rand
}

// Rand is an abstraction over math.Rand.
type Rand interface {
	Intn(int) int
	Float64() float64
}

// New returns new Vose RNG given a uniform RNG and a probability distribution dist.
func New(uniform Rand, dist []float64) *Vose {
	if len(dist) == 0 {
		panic("distribution is empty")
	}
	var sum float64
	for _, p := range dist {
		sum += p
	}
	if sum == 0 {
		panic("probability is zero")
	}

	n := len(dist)
	scaled := make([]float64, n)

	var small, large []int
	for i, p := range dist {
		scaled[i] = p * float64(n) / sum
		if scaled[i] < 1 {
			small = append(small, i)
		} else {
			large = append(large, i)
		}
	}
	prob := make([]float64, n)
	alias := make([]int, n)

	var s, l int
	for len(small) > 0 && len(large) > 0 {
		s, small = small[len(small)-1], small[:len(small)-1]
		l, large = large[len(large)-1], large[:len(large)-1]
		prob[s] = scaled[s]
		alias[s] = l
		scaled[l] = (scaled[l] + scaled[s]) - 1
		if scaled[l] < 1 {
			small = append(small, l)
		} else {
			large = append(large, l)
		}
	}

	for _, i := range large {
		prob[i] = 1
	}
	for _, i := range small {
		prob[i] = 1
	}

	return &Vose{
		prob:  prob,
		alias: alias,
		rand:  uniform,
	}
}

// Rand returns a random integer in [0,n) where n is the length of the probability
// distribution passed to New.
func (v *Vose) Rand() int {
	i := v.rand.Intn(len(v.prob))
	if v.prob[i] >= v.rand.Float64() {
		return i
	}
	return v.alias[i]
}
