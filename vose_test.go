// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

package vose

import (
	"math"
	"math/rand"
	"testing"

	exprand "golang.org/x/exp/rand"
)

// Source is an adapter for using x/exp/rand.Source with math/rand.
type Source struct {
	src exprand.Source
}

func (s *Source) Uint64() uint64 {
	return s.src.Uint64()
}

func (s *Source) Int63() int64 {
	return int64(s.src.Uint64() &^ (1 << 63))
}

func (s *Source) Seed(seed int64) {
	s.src.Seed(uint64(seed))
}

// PCGSource is an adapter for using x/exp/rand.PCGSource with math/rand.
type PCGSource struct {
	src exprand.PCGSource
}

func (s *PCGSource) Uint64() uint64 {
	return s.src.Uint64()
}

func (s *PCGSource) Int63() int64 {
	return int64(s.src.Uint64() &^ (1 << 63))
}

func (s *PCGSource) Seed(seed int64) {
	s.src.Seed(uint64(seed))
}

func TestRand(t *testing.T) {
	uniform := rand.New(rand.NewSource(1))
	vose := New(uniform, []float64{0.3, 0.7})
	var f [2]int
	for i := 0; i < 1000000; i++ {
		f[vose.Rand()]++
	}
	freq := float64(f[0]) / float64(f[0]+f[1])
	if math.Abs(freq-0.3) > 1e-3 {
		t.Errorf("want probability ~%f, got %f", 0.3, freq)
	}
}

func TestRandNormalize(t *testing.T) {
	uniform := rand.New(rand.NewSource(1))
	vose := New(uniform, []float64{3, 1})
	var f [2]int
	for i := 0; i < 1000000; i++ {
		f[vose.Rand()]++
	}
	freq := float64(f[0]) / float64(f[0]+f[1])
	if math.Abs(freq-0.75) > 1e-3 {
		t.Errorf("want probability ~%f, got %f", 0.25, freq)
	}
}

func TestRandZeroProb(t *testing.T) {
	uniform := rand.New(rand.NewSource(1))
	vose := New(uniform, []float64{0, 1, 2, 3, 4})
	var f [5]int
	for i := 0; i < 1000000; i++ {
		f[vose.Rand()]++
	}
	if f[0] != 0 {
		t.Errorf("want zero frequency, got %d", f[0])
	}
}

func BenchmarkNew(b *testing.B) {
	a := make([]float64, 100)
	for i := 0; i < 100; i++ {
		a[i] = float64(i)
	}
	uniform := rand.New(rand.NewSource(1))

	for i := 0; i < b.N; i++ {
		New(uniform, a)
	}
}

func BenchmarkRand(b *testing.B) {
	b.StopTimer()
	a := make([]float64, 100)
	for i := 0; i < 100; i++ {
		a[i] = float64(i)
	}
	uniform := rand.New(rand.NewSource(1))
	vose := New(uniform, a)
	b.StartTimer()

	for i := 0; i < b.N; i++ {
		vose.Rand()
	}
}

func BenchmarkRandPCGSource(b *testing.B) {
	b.StopTimer()
	a := make([]float64, 100)
	for i := 0; i < 100; i++ {
		a[i] = float64(i)
	}
	uniform := rand.New(&Source{exprand.NewSource(1)})
	vose := New(uniform, a)
	b.StartTimer()

	for i := 0; i < b.N; i++ {
		vose.Rand()
	}
}

func BenchmarkRandPCGSourceWithoutInterface(b *testing.B) {
	b.StopTimer()
	a := make([]float64, 100)
	for i := 0; i < 100; i++ {
		a[i] = float64(i)
	}
	var src exprand.PCGSource
	src.Seed(1)
	uniform := rand.New(&PCGSource{src})
	vose := New(uniform, a)
	b.StartTimer()

	for i := 0; i < b.N; i++ {
		vose.Rand()
	}
}
