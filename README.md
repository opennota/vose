vose [![License](http://img.shields.io/:license-gpl3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0.html) [![GoDoc](http://godoc.org/gitlab.com/opennota/vose?status.svg)](http://godoc.org/gitlab.com/opennota/vose) [![Pipeline status](https://gitlab.com/opennota/vose/badges/master/pipeline.svg)](https://gitlab.com/opennota/vose/commits/master)
====

Package vose implements [Walker-Vose alias algorithm](http://www.keithschwarz.com/darts-dice-coins/)
for generating discrete random numbers with a given probability
distribution.


## Install

    go get -u gitlab.com/opennota/vose

